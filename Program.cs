﻿using System;

namespace IsLucky
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        static bool isLucky(int n) {
            string m = Convert.ToString(n);
            int l = m.Length;
            int firstPart = 0;
            int secondPart = 0;

            for (int i = 0; i < l; i++)
            {
                if(i >= l / 2){
                    secondPart += Convert.ToInt32(m[i]);
                }else{
                    firstPart += Convert.ToInt32(m[i]);
                }
            }

            if(firstPart == secondPart){
                return true;
            }else{
                return false;
            }
        }
    }
}
